﻿using UnityEngine;

public class Salto : MonoBehaviour
{
    Rigidbody cuerpoRigido;
    bool isJump = false;
    public float fuerzaSalto = 10.0f;
    bool floorDetected = false;


    void Start()
    {
        cuerpoRigido = GetComponent<Rigidbody>();
    }


    void Update()
    {
        Vector3 floor = transform.TransformDirection(Vector3.down);
        if (Physics.Raycast(transform.position, floor, 1.03f))
        {
            floorDetected = true;

        }
        else
        {
            floorDetected = false;

        }

        isJump = Input.GetButtonDown("Jump");
        if (isJump && floorDetected)
        {
            cuerpoRigido.AddForce(new Vector3(0, fuerzaSalto, 0), ForceMode.Impulse);
        }

    }


}