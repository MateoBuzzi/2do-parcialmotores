﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlJapo : MonoBehaviour
{
    public int rapidez;
    public Animator ani;
    public GameObject target;
    public ControlJugador jugador;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance(transform.position, target.transform.position) < 10)
            {


            var lookPos = target.transform.position - transform.position;
            lookPos.y = 0;
            var rotation = Quaternion.LookRotation(lookPos);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, rotation, 3);
            transform.Translate(Vector3.forward * 5 * Time.deltaTime);
            
        }
    }
}
