﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class Eventos : MonoBehaviour
{
 public  void ReplayGame()
    {
        SceneManager.LoadScene("Run");
    }
    public void QuitGame()
    {
        Application.Quit();
    }
}
