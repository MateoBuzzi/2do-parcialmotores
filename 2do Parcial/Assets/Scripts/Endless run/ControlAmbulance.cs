﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class ControlAmbulance : MonoBehaviour
{
    private CharacterController controller;
    private Vector3 direction;
    public float forwardSpeed;

    private int desiredLine = 1;
    public float laneDistance = 3;
    public float jumpForce;
    public float Gravity = -9;
    public float maxSpeed;
   
   
    
    void Start()
    {
        controller = GetComponent<CharacterController>();
        GestorDeAudio.instancia.ReproducirSonido("MusicaA");

    }

    
    void Update()
    {
        direction.z = forwardSpeed;
        direction.y += Gravity * Time.deltaTime; 
        if(controller.isGrounded)
        {
            if (Input.GetKeyDown(KeyCode.W))
            {
                Jump();
            }
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene("Run");
        }
        if (forwardSpeed < maxSpeed)
        forwardSpeed += 0.25f * Time.deltaTime;

        if(Input.GetKeyDown(KeyCode.D))
        {
            desiredLine++;
            if (desiredLine == 3)
                desiredLine = 2;
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            desiredLine--;
            if (desiredLine == -1)
                desiredLine = 0;
        }
        Vector3 targetPosition = transform.position.z * transform.forward + transform.position.y * transform.up;

        if(desiredLine == 0)
        {
            targetPosition += Vector3.left * laneDistance;
        }
        else if(desiredLine ==2)
        {
            targetPosition += Vector3.right * laneDistance;
        }

        transform.position = targetPosition;
        controller.center = controller.center;
    }
    private void FixedUpdate()
    {
        controller.Move(direction * Time.fixedDeltaTime);
    }
    private void Jump()
    {
        direction.y = jumpForce;
    }
    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if(hit.transform.tag == "Obstaculo")
        {
            PlayerManager.gamerOver = true;
        }
    }
}
