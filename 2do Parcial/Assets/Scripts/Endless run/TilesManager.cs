﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TilesManager : MonoBehaviour
{
    public GameObject[] tilePrefab;
    public float zSpawn = 0;
    public float tileLenght = 30;
    public int numberTiles = 5;
    public Transform playerTransform;
    private int MaxTiles = 10;
    private List<GameObject> activeTiles = new List<GameObject>();
    void Start()
    {
        
            
        for(int i = 0; i < numberTiles; i++)
        {
            if (i == 0 )
                SpawnTile(0);
            else
            SpawnTile(Random.Range(0, tilePrefab.Length));
        
        }
    }
    private void Update()
    {
        if (playerTransform.position.z -35 > zSpawn - (numberTiles * tileLenght))
        {
            SpawnTile(Random.Range(0, tilePrefab.Length));
            DeleteTiles();
        }
    }

    public void SpawnTile(int tileIndex)
    {
        GameObject go =  Instantiate(tilePrefab[tileIndex], transform.forward * zSpawn, transform.rotation);
        activeTiles.Add(go);
        zSpawn += tileLenght;
} 
    private void DeleteTiles()
    {
        Destroy(activeTiles[0]);
        activeTiles.RemoveAt(0);
    }

}







