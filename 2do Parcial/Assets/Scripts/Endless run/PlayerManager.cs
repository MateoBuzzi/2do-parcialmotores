﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    public static bool gamerOver;
    public GameObject gamerOverPanel;
    public static int numberOfCoins;
    public Text Cointxt;
    private int MaxCoins = 200;
    void Start()
    {
        gamerOver = false;
        Time.timeScale = 1;
        numberOfCoins = 0;
    }

  
    void Update()
    {
        if (gamerOver)
        {
            Time.timeScale = 0;
            gamerOverPanel.SetActive(true);
        }
        Cointxt.text = "Monedas :" + numberOfCoins;

        if(numberOfCoins == MaxCoins)
        {
            SceneManager.LoadScene("EscenaFinal");
        }
    }
   
}
