﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlMenuPrincipal : MonoBehaviour
{
    public void Jugar()
    {
        SceneManager.LoadScene("PrimerSave");
    }
    public void Salir()
    {
        Debug.Log("Adios");
        Application.Quit();
}
}
