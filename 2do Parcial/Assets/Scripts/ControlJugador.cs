﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ControlJugador : MonoBehaviour
{

    public float rapidezDesplazamiento = 9.0f;
    public Camera camaraEnPrimeraPersona;
    public GameObject proyectil;
    public float Recarga = 2.9f;
    private float Disparo = Mathf.NegativeInfinity;
    public float Recarga2 = 2;
    public Text textoLlave;
    private int cont = 0;
    public GameObject Porton;
    public GameObject textocaza;
    public Text textoCocina;
    public int vida = 100;
    public Image barra;
    public Text textococ;
    public Text encontro;
    public Text busca;
    public GameObject reja;
    bool estajapo = false;
    public GameObject Audio;
    public GameObject Pj;
    

   


    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        GestorDeAudio.instancia.ReproducirSonido("Musica1");


    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("llave") == true)
        {
            cont = cont + 1;
            setearTexto();
            other.gameObject.SetActive(false);
            AbrirPuertas();

        }
        if (other.gameObject.CompareTag("carneconejo") == true)
        {
            Destroy(textocaza);
            cocinalo();
        }
        if (other.gameObject.CompareTag("horno")== true )
        {
            cocinaste();
        }
        if (other.gameObject.CompareTag("Japo") == true)
        {
            japo();
            estajapo = true;
        }
        if(other.gameObject.CompareTag("ambu") == true && estajapo == true)
        {
            SceneManager.LoadScene("Run");
            Destroy(Audio);
            Destroy(Pj);
        }

        
       if (other.CompareTag("arma") == true)
         {
            dañoo();
            Debug.Log("AiA");
         }
        
    }
    private void dañoo()
    {
        vida = vida - 50;
    }
    private void cocinaste()
    {
        textococ.text = "Cocinaste";
        Destroy(textoCocina);
    }    
    private void japo()
    {
        encontro.text = "Escapa en la ambulancia con Japo";
            Destroy(busca);
        Destroy(reja);
    }
    
    private void cocinalo()
    {
        textoCocina.text = "-Cocina tu alimento";
    }

    private void setearTexto()
    {
        textoLlave.text = "-Encontraste una llave ";
    }

    private void AbrirPuertas()
    {
        Destroy(Porton);
    }



        void Update()
    {
        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento;
        float movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento;


        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;

        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);

        if (Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;
        }


        if (Input.GetMouseButtonDown(0) && Disparo + Recarga * Recarga2 < Time.time)
        {
            Ray rayo = camaraEnPrimeraPersona.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0f));

            GameObject pro;
            pro = Instantiate(proyectil, rayo.origin, transform.rotation);

            Rigidbody rb = pro.GetComponent<Rigidbody>();
            rb.AddForce(camaraEnPrimeraPersona.transform.forward * 80, ForceMode.Impulse);
            Disparo = Time.time;
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
        if(vida <= 0)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

    }




}
