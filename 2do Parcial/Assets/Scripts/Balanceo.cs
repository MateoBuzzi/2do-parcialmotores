﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Balanceo : MonoBehaviour
{
    private Quaternion originLocalRotation;

    
   private void Start()
    {
        originLocalRotation = transform.localRotation;
    }

    
    void Update()
    {
        updateBalanceo();
    }

    private void updateBalanceo()
    {
        float t_xLookInput = Input.GetAxis("Mouse X");
        float t_yLookInput = Input.GetAxis("Mouse Y");

        Quaternion t_xAngleAdjustment = Quaternion.AngleAxis(-t_xLookInput * 3.45f, Vector3.up);
        Quaternion t_yAngleAdjustment = Quaternion.AngleAxis(t_yLookInput * 3.45f, Vector3.right);
        Quaternion t_targetRotation = originLocalRotation * t_xAngleAdjustment * t_yAngleAdjustment;

        transform.localRotation = Quaternion.Lerp(transform.localRotation, t_targetRotation, Time.deltaTime * 10f);
    }
}
