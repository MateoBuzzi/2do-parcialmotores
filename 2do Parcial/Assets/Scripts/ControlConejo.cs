﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlConejo : MonoBehaviour
{
    public int rutina;
    public float cronometro;
    public Animator ani;
    public Quaternion angulo;
    public float grado;
    private int hp;
    public GameObject CarneConejo;
    public Transform transformer;

    public void recibirDaño()
    {
        hp = hp - 100;

       
    }
  
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("bala"))
        {
            recibirDaño();
        }
    }
    void Start()
    {
        ani = GetComponent<Animator>();
        hp = 100;
    }


    void Update()
    {
        if (hp == 100)
        {
            Comportamiento_Enemigo();
        }
        else
        {
            MuerteConejo();
            Drop();
        }
    }
    public void Comportamiento_Enemigo()
    {
        cronometro += 1 * Time.deltaTime;
        if (cronometro >=4)
        {
            rutina = Random.Range(0, 2);
            cronometro = 0;
        }
        switch (rutina)
        {
            case 0:
                ani.SetBool("walk", false);
                break;
            case 1:
                grado = Random.Range(0, 360);
                angulo = Quaternion.Euler(0, grado, 0);
                rutina++;
                break;
            case 2:
                transform.rotation = Quaternion.RotateTowards(transform.rotation, angulo, 0.5f);
                transform.Translate(Vector3.forward * 2 * Time.deltaTime);
                ani.SetBool("walk", true);
                break;






        }
    }
    public void MuerteConejo()
    {
        ani.SetBool("dead", true);
      

    }
    void Drop()
    {
        Vector3 position = transform.position;
        GameObject CarnedeConejo = Instantiate(CarneConejo, position, Quaternion.identity);
        CarnedeConejo.SetActive(true);
        Destroy(CarnedeConejo, 1f);

    }
}
