﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlZombie : MonoBehaviour
{
    public int rutina;
    public float cronometro;
    public Animator ani;
    public Quaternion angulo;
    public float grado;
    private int hp;
    public GameObject target;
    public bool atacando;



    public void recibirDaño()
    {
        hp = hp - 50;


    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("bala"))
        {
            recibirDaño();
            Debug.Log("se la puse");
        }
    }
    void Start()
    {
        
        ani = GetComponent<Animator>();
        hp = 100;
        target = GameObject.Find("Jugador");
    }


    void Update()
    {
        Comportamiento_Pasivo();



        if (hp == 0)
        {
            Destroy(gameObject);
        }
    }
    public void Comportamiento_Pasivo()
    {
        if (Vector3.Distance(transform.position, target.transform.position) > 20)
        {
            ani.SetBool("caminar", false);
            cronometro += 1 * Time.deltaTime;
            if (cronometro >= 4)
            {
                rutina = Random.Range(0, 2);
                cronometro = 0;
            }
            switch (rutina)
            {
                case 0:
                    ani.SetBool("caminar", false);
                    break;
                case 1:
                    grado = Random.Range(0, 360);
                    angulo = Quaternion.Euler(0, grado, 0);
                    rutina++;
                    break;
                case 2:
                    transform.rotation = Quaternion.RotateTowards(transform.rotation, angulo, 0.5f);
                    transform.Translate(Vector3.forward * 2 * Time.deltaTime);
                    ani.SetBool("caminar", true);
                    break;
            }
        }
        else
        {
            if (Vector3.Distance(transform.position, target.transform.position) > 2 && !atacando)
            {
                var lookPos = target.transform.position - transform.position;
                lookPos.y = 0;
                var rotation = Quaternion.LookRotation(lookPos);
                transform.rotation = Quaternion.RotateTowards(transform.rotation, rotation, 3);
                ani.SetBool("caminar", false);

                ani.SetBool("run", true);
                transform.Translate(Vector3.forward * 2 * Time.deltaTime);
                ani.SetBool("atacar", false);
            }
            else
            {
                ani.SetBool("caminar", false);
                ani.SetBool("run", false);
                ani.SetBool("atacar", true);
                atacando = true;
            }
        }
    }
        public void Final_Ani()
        {
        ani.SetBool("atacar", false);
        atacando = false;
        }
    


}